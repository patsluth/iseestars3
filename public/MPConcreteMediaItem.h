




@interface MPConcreteMediaItem : NSObject

+ (id)concreteMediaItemWithPersistentID:(unsigned long long)arg1;
+ (id)concreteMediaItemWithPersistentID:(unsigned long long)arg1 library:(id)arg2;

- (id)valueForEntityProperty:(id)arg1;
- (id)valueForProperty:(id)arg1;
- (id)valuesForProperties:(id)arg1;

@end




