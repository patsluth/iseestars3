//
//  SWISeeStars.xm
//  ISeeStars3
//
//  Created by Pat Sluth on 2015-12-27.
//  Copyright © 2017 Pat Sluth. All rights reserved.
//

#import "SWISeeStarsController.h"
#import "SWISeeStarsView.h"

#import "Sluthware/Sluthware.h"

#import "MPConcreteMediaItem.h"
#import "MPModelResponse.h"
#import "MPLazySectionedCollection.h"
#import "MPModelObject.h"
#import "MPModelPlaylistEntry.h"
#import "MPModelSong.h"
#import "MPIdentifierSet.h"
#import "_TtGC5Music30BrowseCollectionViewControllerCSo16UICollectionView_.h"

#import <objc/runtime.h>
#include <dlfcn.h>





@interface _TtC5Music33PlaylistDetailSongsViewController : _TtGC5Music30BrowseCollectionViewControllerCSo16UICollectionView_

@end



@interface _TtC5Music30AlbumDetailSongsViewController : _TtGC5Music30BrowseCollectionViewControllerCSo16UICollectionView_

@end



@interface _TtC5Music19SongsViewController : _TtGC5Music30BrowseCollectionViewControllerCSo16UICollectionView_

@end



@interface _TtC5Music24NowPlayingViewController : UIViewController

@end





static MPConcreteMediaItem *getConcreteMediaItem(MPModelResponse *modelResponse, NSIndexPath *indexPath)
{
	MPSectionedCollection *results = modelResponse.results;
	MPModelObject *modelObject = [results itemAtIndexPath:indexPath];
	
	MPModelSong *modelSong = nil;
	
	
	if ([modelObject isKindOfClass:%c(MPModelPlaylistEntry)]) {
		
		MPModelPlaylistEntry *modelPlaylistEntry = (MPModelPlaylistEntry *)modelObject;
		modelSong = modelPlaylistEntry.song;
		
	} else if ([modelObject isKindOfClass:%c(MPModelSong)]) {
		
		modelSong = (MPModelSong *)modelObject;
	}
	
	
	if (modelSong) {
		
		MPIdentifierSet *identifiers = modelSong.identifiers;
		NSInteger persistentID = identifiers.deviceLibraryPersistentID;
		
		MPConcreteMediaItem *concreteMediaItem = [%c(MPConcreteMediaItem) concreteMediaItemWithPersistentID:persistentID];
		return concreteMediaItem;
	}
	
	return nil;
}

static void updateISeeStarsView(UIView *superView, MPConcreteMediaItem *concreteMediaItem)
{
	SWISeeStarsView *iSeeStarsView = [[SWISeeStarsController sharedInstance] getISeeStarsViewForObject:superView];
	
	if ([concreteMediaItem respondsToSelector:@selector(valueForEntityProperty:)]) {
		
		//		NSString *itemTitle = [concreteMediaItem valueForEntityProperty:@"title"];
		NSNumber *itemRating = [concreteMediaItem valueForEntityProperty:@"rating"];
		NSNumber *itemLikedState = [concreteMediaItem valueForEntityProperty:@"likedState"];
		
		//		NSLog(@"itemTitle %@\titemRating %@\titemLikedState %@", itemTitle, itemRating, itemLikedState);
		
		if (!iSeeStarsView) {
			
			iSeeStarsView = [[SWISeeStarsController sharedInstance] newISeeStarsView];
			
			if (iSeeStarsView) {
				
				[superView addSubview:iSeeStarsView];
				
				[superView addConstraint:[NSLayoutConstraint constraintWithItem:iSeeStarsView
																 attribute:NSLayoutAttributeLeading
																 relatedBy:NSLayoutRelationEqual
																	toItem:superView
																 attribute:NSLayoutAttributeLeading
																multiplier:1.0
																  constant:0.0]];
				[superView addConstraint:[NSLayoutConstraint constraintWithItem:iSeeStarsView
																 attribute:NSLayoutAttributeTop
																 relatedBy:NSLayoutRelationEqual
																	toItem:superView
																 attribute:NSLayoutAttributeTop
																multiplier:1.0
																  constant:0.0]];
				[superView addConstraint:[NSLayoutConstraint constraintWithItem:iSeeStarsView
																 attribute:NSLayoutAttributeWidth
																 relatedBy:NSLayoutRelationEqual
																	toItem:nil
																 attribute:NSLayoutAttributeNotAnAttribute
																multiplier:1.0
																  constant:16.0]]; //artwork origin (taken from flex)
				[superView addConstraint:[NSLayoutConstraint constraintWithItem:iSeeStarsView
																 attribute:NSLayoutAttributeBottom
																 relatedBy:NSLayoutRelationEqual
																	toItem:superView
																 attribute:NSLayoutAttributeBottom
																multiplier:1.0
																  constant:0.0]];
				
				[superView setNeedsLayout];
				[iSeeStarsView setNeedsLayout];
			}
			
			[[SWISeeStarsController sharedInstance] setISeeStarsView:iSeeStarsView forObject:superView withPolicy:OBJC_ASSOCIATION_RETAIN_NONATOMIC];
		}
		
		iSeeStarsView.rating = [itemRating integerValue];
		iSeeStarsView.likedStatus = [itemLikedState integerValue];
		
	} else {
		
		[iSeeStarsView removeFromSuperview];
		[[SWISeeStarsController sharedInstance] removeISeeStarsView:iSeeStarsView forObject:superView];
		
	}
}





%group Music_PlaylistDetailSongsViewController

%hook _TtC5Music33PlaylistDetailSongsViewController

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
//	SWLogMethod_Start
	
	UICollectionViewCell *cell = %orig(collectionView, indexPath);
	
	if ([cell isKindOfClass:%c(_TtC5Music8SongCell)]) {
		
		if ([self respondsToSelector:@selector(modelResponse)] &&
			[self.modelResponse isKindOfClass:%c(MPModelResponse)] &&
			[self isModelObjectForItemAt:indexPath]) {
			
			MPModelResponse *modelResponse = (MPModelResponse *)self.modelResponse;
			NSIndexPath *modelIndexPath = [self convertToModelIndexPath:indexPath];
			MPConcreteMediaItem *concreteMediaItem = getConcreteMediaItem(modelResponse, modelIndexPath);
			updateISeeStarsView(cell, concreteMediaItem);
			
		}
		
	}
	
//	SWLogMethod_End
	
	return cell;
}

%end

%end





%group Music_AlbumDetailSongsViewController

%hook _TtC5Music30AlbumDetailSongsViewController

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
	//	SWLogMethod_Start
	
	UICollectionViewCell *cell = %orig(collectionView, indexPath);
	
	if ([cell isKindOfClass:%c(_TtC5Music8SongCell)]) {
		
		if ([self respondsToSelector:@selector(modelResponse)] &&
			[self.modelResponse isKindOfClass:%c(MPModelResponse)] &&
			[self isModelObjectForItemAt:indexPath]) {
			
			MPModelResponse *modelResponse = (MPModelResponse *)self.modelResponse;
			NSIndexPath *modelIndexPath = [self convertToModelIndexPath:indexPath];
			MPConcreteMediaItem *concreteMediaItem = getConcreteMediaItem(modelResponse, modelIndexPath);
			updateISeeStarsView(cell, concreteMediaItem);
			
		}
		
	}
	
	//	SWLogMethod_End
	
	return cell;
}

%end

%end





%group Music_SongsViewController

%hook _TtC5Music19SongsViewController

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
//	SWLogMethod_Start
	
	UICollectionViewCell *cell = %orig(collectionView, indexPath);
	
	if ([cell isKindOfClass:%c(_TtC5Music8SongCell)]) {
		
		if ([self respondsToSelector:@selector(modelResponse)] &&
			[self.modelResponse isKindOfClass:%c(MPModelResponse)] &&
			[self isModelObjectForItemAt:indexPath]) {
			
			MPModelResponse *modelResponse = (MPModelResponse *)self.modelResponse;
			NSIndexPath *modelIndexPath = [self convertToModelIndexPath:indexPath];
			MPConcreteMediaItem *concreteMediaItem = getConcreteMediaItem(modelResponse, modelIndexPath);
			updateISeeStarsView(cell, concreteMediaItem);
			
		}
		
	}

//	SWLogMethod_End

	return cell;
}

%end

%end





%group Music_NowPlayingViewController

%hook _TtC5Music24NowPlayingViewController

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
//	SWLogMethod_Start
	
	UICollectionViewCell *cell = %orig(collectionView, indexPath);
	
	if ([cell isKindOfClass:%c(_TtC5Music8SongCell)]) {
		
		MPModelResponse *modelResponse = MSHookIvar<MPModelResponse *>(self, "queueResponse");
		MPConcreteMediaItem *concreteMediaItem = getConcreteMediaItem(modelResponse, indexPath);
		updateISeeStarsView(cell, concreteMediaItem);
		
	}
	
//	SWLogMethod_End
	
	return cell;
}

%end

%end





static BOOL Music_PlaylistDetailSongsViewController_Hooked = NO;
static BOOL Music_AlbumDetailSongsViewController_Hooked = NO;
static BOOL Music_SongsViewController_Hooked = NO;
static BOOL Music_NowPlayingViewController_Hooked = NO;





%hook UIViewController

- (void)viewDidLoad
{
	if (!Music_PlaylistDetailSongsViewController_Hooked && [self class] == %c(_TtC5Music33PlaylistDetailSongsViewController)) {
		
		Music_PlaylistDetailSongsViewController_Hooked = YES;
		NSLog(@"Initialize hooks for %@", [self class]);
		%init(Music_PlaylistDetailSongsViewController);
		
	} else if (!Music_AlbumDetailSongsViewController_Hooked && [self class] == %c(_TtC5Music30AlbumDetailSongsViewController)) {
		
		Music_AlbumDetailSongsViewController_Hooked = YES;
		NSLog(@"Initialize hooks for %@", [self class]);
		%init(Music_AlbumDetailSongsViewController);
		
	} else if (!Music_SongsViewController_Hooked && [self class] == %c(_TtC5Music19SongsViewController)) {
		
		Music_SongsViewController_Hooked = YES;
		NSLog(@"Initialize hooks for %@", [self class]);
		%init(Music_SongsViewController);
		
	} else if (!Music_NowPlayingViewController_Hooked && [self class] == %c(_TtC5Music24NowPlayingViewController)) {
		
		Music_NowPlayingViewController_Hooked = YES;
		NSLog(@"Initialize hooks for %@", [self class]);
		%init(Music_NowPlayingViewController);
		
	}
	
	%orig();
}

%end





//%hookf(Class *, objc_allocateClassPair, Class superclass, const char *name, size_t extraBytes)
//{
//	NSLog(@"objc_allocateClassPair: %s", name);
//
//	return %orig;
//}





%ctor
{
	loadSluthware();
	
	%init(_ungrouped);
}




