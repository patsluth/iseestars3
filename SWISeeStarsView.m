//
//  SWISeeStarsView.m
//  ISeeStars3
//
//  Created by Pat Sluth on 2016-04-23.
//
//

#import "SWISeeStarsView.h"

#import "SWISeeStarsController.h"

#import "Sluthware/Sluthware.h"





@interface SWISeeStarsView()

@property (strong, nonatomic) IBOutletCollection(UIImageView) NSArray<UIImageView *> *imageViewCollection;

@end





@implementation SWISeeStarsView

#pragma mark - Init

- (void)awakeFromNib
{
	[super awakeFromNib];
	
	self.translatesAutoresizingMaskIntoConstraints = NO;
	
	self.rating = 0;
	self.likedStatus = 0;
}

#pragma mark - SWISeeStarsView

- (void)setRating:(NSInteger)rating
{
	_rating = MAX(0, MIN(5, rating));
	
	for (NSUInteger x = 0; x < self.imageViewCollection.count; x++) {
		
		UIImageView *imageView = [self.imageViewCollection objectAtIndex:x];
		
		if (x >= _rating) {
			imageView.image = [SWISeeStarsController sharedInstance].dotImage;
		} else {
			if (self.likedStatus == 2) { // 2 = isLiked
				imageView.image = [SWISeeStarsController sharedInstance].heartImage;
			} else {
				imageView.image = [SWISeeStarsController sharedInstance].starImage;
			}
		}
	}
}

- (void)setLikedStatus:(NSInteger)likedStatus
{
	_likedStatus = likedStatus;
	
	if (_likedStatus == 2) { // 2 = isLiked
		if (self.rating <= 0) { // Force show a single heart
			self.rating = 1;
			return;
		}
	}
	
	self.rating = self.rating; // Update views
}

//- (void)setFrame:(CGRect)frame
//{
//	[super setFrame:frame];
//	
//	CGSize desiredImageSize = self.imageViewCollection.firstObject.bounds.size;
//	desiredImageSize.height = MAX(desiredImageSize.width, desiredImageSize.height); // square
//	
//	if (!CGSizeEqualToSize(desiredImageSize, self.dotImage.size)) { // Only resize if we need to
//		
//		DISPATCH_ASYNC_BACKGROUND_QUEUE
//			
//		self.dotImage = [[self.dotImage resizeImage:desiredImageSize] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
//		self.starImage = [[self.starImage resizeImage:desiredImageSize] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
//		self.heartImage = [[self.heartImage resizeImage:desiredImageSize] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
//		
//		DISPATCH_ASYNC_MAIN_QUEUE
//		
//		self.likedStatus = self.likedStatus;
//		
//		DISPATCH_ASYNC_MAIN_QUEUE_END
//		
//		DISPATCH_ASYNC_BACKGROUND_QUEUE_END
//	}
//}

@end




