//
//  SWISeeStarsController.m
//  ISeeStars3
//
//  Created by Pat Sluth on 2016-04-23.
//
//

#import "SWISeeStarsController.h"

#import "SWISeeStarsView.h"

#import "Sluthware/Sluthware.h"





@interface SWISeeStarsController()

@property (strong, nonatomic, readwrite) UIImage *dotImage;
@property (strong, nonatomic, readwrite) UIImage *starImage;
@property (strong, nonatomic, readwrite) UIImage *heartImage;

@property (strong, nonatomic, readwrite) NSBundle *bundle;
@property (strong, nonatomic, readwrite) SWPrefs *prefs;

@end





@implementation SWISeeStarsController

#pragma mark - Init

static SWISeeStarsController *_sharedInstance = nil;

+ (instancetype)sharedInstance
{
	@synchronized(self) {
		if (_sharedInstance == nil) {
			_sharedInstance = [[self alloc] init];
		}
	}
	
	return _sharedInstance;
}

- (id)init
{
	if (!_sharedInstance && self == [super init]) {
		
		self.bundle = [NSBundle bundleWithPath:@"/Library/Application Support/ISeeStarsSupport.bundle"];
		
		NSBundle *preferenceBundle = [NSBundle bundleWithPath:@"/Library/PreferenceBundles/ISeeStars3_PreferencesBundle.bundle"];
		NSString *preferencePath = @"/var/mobile/Library/Preferences/com.patsluth.iseestars3.plist";
		NSString *defaultsPath = [preferenceBundle pathForResource:@"prefsDefaults" ofType:@".plist"];
		
		self.prefs = [[SWPrefs alloc] initWithPreferenceFilePath:preferencePath
													defaultsPath:defaultsPath
													 application:@"com.patsluth.iseestars3"];
		[self.prefs refreshPrefs];
		
		[[NSNotificationCenter defaultCenter] addObserver:self
												 selector:@selector(applicationDidBecomeActive:)
													 name:UIApplicationDidBecomeActiveNotification
												   object:nil];
	}
	
	return self;
}

#pragma mark - SWISeeStarsController

- (UIImage *)dotImage
{
	BOOL showDots = [[self.prefs.preferences valueForKey:@"showDots"] boolValue];
	
	return (showDots) ? _dotImage : nil;
}

- (void)setBundle:(NSBundle *)bundle
{
	_bundle = bundle;
	
	UIImage *dotImage = [UIImage imageWithContentsOfFile:[bundle pathForResource:@"Dot" ofType:@"png"]];
	self.dotImage = [dotImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
	UIImage *starImage = [UIImage imageWithContentsOfFile:[bundle pathForResource:@"Star" ofType:@"png"]];
	self.starImage = [starImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
	UIImage *heartImage = [UIImage imageWithContentsOfFile:[bundle pathForResource:@"Heart" ofType:@"png"]];
	self.heartImage = [heartImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
}

- (SWISeeStarsView *)newISeeStarsView
{
	NSArray *nibViews = [self.bundle loadNibNamed:@"SWISeeStarsView" owner:nil options:nil];
	
	if (nibViews.count > 0 && [nibViews.firstObject isKindOfClass:[SWISeeStarsView class]]) {
		return (SWISeeStarsView *)nibViews.firstObject;
	}
	
	return nil;
}

- (SWISeeStarsView *)getISeeStarsViewForObject:(id)object
{
	return objc_getAssociatedObject(object, @selector(getISeeStarsViewForObject:));
}

- (void)setISeeStarsView:(SWISeeStarsView *)iSeeStarsView forObject:(id)object withPolicy:(objc_AssociationPolicy)policy
{
	objc_setAssociatedObject(object, @selector(getISeeStarsViewForObject:), iSeeStarsView, policy);
}

- (void)removeISeeStarsView:(SWISeeStarsView *)iSeeStarsView forObject:(id)object
{
	[self setISeeStarsView:nil forObject:object withPolicy:OBJC_ASSOCIATION_RETAIN_NONATOMIC];
}

#pragma mark - NSNotificationCenter

- (void)applicationDidBecomeActive:(NSNotification *)notification
{
	[self.prefs refreshPrefs];
}

@end




