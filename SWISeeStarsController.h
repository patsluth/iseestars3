//
//  SWISeeStarsController.h
//  ISeeStars3
//
//  Created by Pat Sluth on 2016-04-23.
//
//

#import <UIKit/UIKit.h>

#import <objc/runtime.h>

@class SWISeeStarsView;
@class SWPrefs;





@interface SWISeeStarsController : NSObject

+ (instancetype)sharedInstance;

- (SWISeeStarsView *)newISeeStarsView;
- (SWISeeStarsView *)getISeeStarsViewForObject:(id)object;
- (void)setISeeStarsView:(SWISeeStarsView *)iSeeStarsView forObject:(id)object withPolicy:(objc_AssociationPolicy)policy;
- (void)removeISeeStarsView:(SWISeeStarsView *)iSeeStarsView forObject:(id)object;

@property (strong, nonatomic, readonly) UIImage *dotImage;
@property (strong, nonatomic, readonly) UIImage *starImage;
@property (strong, nonatomic, readonly) UIImage *heartImage;

@property (strong, nonatomic, readonly) SWPrefs *prefs;

@end




