




export FINALPACKAGE = 1
PACKAGE_VERSION = 1.0-3





ifeq ($(FINALPACKAGE), 0)
	export ARCHS = arm64
else
    export ARCHS = armv7 armv7s arm64
endif
export TARGET = iphone:clang:latest:8.0





TWEAK_NAME = ISeeStars3

ISeeStars3_CFLAGS += -fobjc-arc
ISeeStars3_LDFLAGS += -F$(THEOS)/lib/
ISeeStars3_FILES = SWISeeStars.xm SWISeeStarsController.m SWISeeStarsView.m

ISeeStars3_FRAMEWORKS = Foundation UIKit CoreGraphics
ISeeStars3_WEAK_FRAMEWORKS = Sluthware
ISeeStars3_LIBRARIES = substrate sw2

export ADDITIONAL_CFLAGS += -I../public
export ADDITIONAL_CFLAGS += -Ipublic
ifeq ($(FINALPACKAGE), 0)
	export ADDITIONAL_CFLAGS += -Wno-unused-variable -Wno-unused-function
endif

INSTALL_TARGET_PROCESSES = Music





BUNDLE_NAME = ISeeStarsSupport
ISeeStarsSupport_INSTALL_PATH = /Library/Application Support





SUBPROJECTS += ISeeStars3_PreferencesBundle





include $(THEOS)/makefiles/common.mk
include $(THEOS)/makefiles/tweak.mk
include $(THEOS)/makefiles/bundle.mk
include $(THEOS)/makefiles/aggregate.mk
include $(THEOS)/makefiles/swcommon.mk




