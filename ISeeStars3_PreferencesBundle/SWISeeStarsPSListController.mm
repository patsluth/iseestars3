//
//  SWISeeStarsPSListController.m
//  ISeeStars3_PreferencesBundle
//
//  Created by Pat Sluth on 2015-04-25.
//
//

#import "SWISeeStarsPSListController.h"

#import <Sluthware/Sluthware.h>





@interface SWISeeStarsPSListController()

@end





@implementation SWISeeStarsPSListController

+ (void)load
{
	[super load];
	
	loadSluthware();
}

/**
 *  Lazy Load prefs
 *
 *  @return prefs
 */
- (SWPrefs *)prefs
{
	if (![super prefs]) {
		
		NSString *preferencePath = @"/var/mobile/Library/Preferences/com.patsluth.iseestars3.plist";
		NSString *defaultsPath = [self.bundle pathForResource:@"prefsDefaults" ofType:@".plist"];
		
		self.prefs = [[SWPrefs alloc] initWithPreferenceFilePath:preferencePath
													defaultsPath:defaultsPath
													 application:@"com.patsluth.iseestars3"];
	}
	
	return [super prefs];
}

@end




