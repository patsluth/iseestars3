//
//  SWISeeStarsView.h
//  ISeeStars3
//
//  Created by Pat Sluth on 2016-04-23.
//
//

#import <UIKit/UIKit.h>

#import <objc/runtime.h>





@interface SWISeeStarsView : UIView

@property (nonatomic) NSInteger rating;
@property (nonatomic) NSInteger likedStatus;

@end




